import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  public item: any;

  /**
   * Permet de créer la page à l'aide de l'objet item passé en paramètre
   * de la requête
   * @param route ...
   * @param router Objet qui permet d'accéder à l'item en paramètre
   */
  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.item = this.router.getCurrentNavigation().extras.state.item;
      }
    });
  }

  ngOnInit() {
  }

}

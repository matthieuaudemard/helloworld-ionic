import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  items = [
    { id: 1, title: 'Item 1', label: 'label 1', img: '/assets/computer.jpg' },
    { id: 2, title: 'Item 2', label: 'label 2', img: '/assets/keyboard.jpg' },
    { id: 3, title: 'Item 3', label: 'label 3', img: '/assets/technology.jpg' }
  ];

  constructor(private router: Router) { }


  ngOnInit() {
  }

  /**
   * Cette méthode sert à enregister l'objet 'data' fourni dans les paramètres
   * de la requête puis d'effectuer un redirection.
   * @param data l'objet à passer en paramètres
   */
  public onClickDetails(data) {
    const navigationExtras: NavigationExtras = {
      state: {
        item: data
      }
    };
    // redirection
    this.router.navigate(['detail'], navigationExtras);
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.page.html',
  styleUrls: ['./page1.page.scss'],
})
export class Page1Page implements OnInit {

  text1 = 'Je suis un texte.';
  numButton = 1;


  constructor() { }

  ngOnInit() {
  }

  public onClickButton1(): void {
    this.text1 = 'Bouton clické.';
    this.numButton = 2;
  }

  public onClickButton2(): void {
    this.text1 = 'Je suis un texte.';
    this.numButton = 1;
  }

}
